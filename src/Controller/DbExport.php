<?php

namespace Drupal\s3_db_export\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\s3_db_export\DbexportManagerInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DisplayTableController.
 *
 * @package Drupal\mydata\Controller
 *
 * @todo Rename class to DatabaseExportS3
 */
class DbExport extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  protected $dbexportManager;

  /**
   * Constructor.
   *
   * @param \Drupal\s3_db_export\DbexportManagerInterface $dbexportManager
   *   Export manager.
   */
  public function __construct(DbexportManagerInterface $dbexportManager) {
    // @todo Rename variables to $dbExportManager in all instances.
    $this->dbexportManager = $dbexportManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('s3_db_export.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function display() {
    $this->dbexportManager->DbExport();
  }

  /**
   * {@inheritdoc}
   */
  public function download() {
    try {
      $filename = $this->dbexportManager->dbDownload();
      $path = \Drupal::service('file_system')->realpath("public://tmp/");
      $headers = [
        'Content-Type' => 'text/sql',
        'Content-Description' => 'File Download',
        'Content-Disposition' => 'attachment; filename=' . $filename,
      ];

      $uri = $path . '/' . $filename;

      return new BinaryFileResponse($uri, 200, $headers, TRUE);
    }
    catch (FileNotFoundException $e) {
      return new Response('Something went wrong.');
    }
    catch (ClientException $e) {
      return new Response('Something went wrong.');
    }
    catch (ServerException $e) {
      return new Response('Something went wrong.');
    }
  }

}
