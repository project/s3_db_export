<?php

namespace Drupal\s3_db_export\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoginRedirectionForm.
 *
 * @package Drupal\sos_common\Form
 */
class DbForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $state;
  /**
   * {@inheritdoc}
   */
  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(StateInterface $state, MessengerInterface $messenger) {
    $this->state = $state;
    $this->messenger = $messenger;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('state'),
          $container->get('messenger')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    // @todo Rename to DatabaseExportS3Form.
    return 'DbForm';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    // @todo Rename to DatabaseExport.settings.
    return ['Dbexport.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @todo No empty comment blocks
   * @todo User more meaningful variable names, avoid naming like
   *       $first_param and $sec_param and name them with what they represent.
   */
  public function variableSet($fist_param, $sec_param) {
    // @todo We need to use \Drupal::config() instead of state()
    return $this->state->set($fist_param, $sec_param);
  }

  /**
   * {@inheritdoc}
   *
   * @todo No empty comment blocks
   * @todo Avoid naming functions that are deprecated from Drupal 7
   *       and make it clear what is the intention of the function.
   */
  public function variableGet($fist_param, $sec_param = "") {
    $r = $this->state->get($fist_param);
    return ($r == NULL) ? $sec_param : $r;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // @todo This is not used anywhere in the code, but it should
    $form['enable_s3_service'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('You want to store DB on AWS S3'),
    ];

    $form['aws_region'] = [
      '#type' => 'textfield',
      '#title' => "AWS Region",
      '#description' => $this->t("aws region"),
      // @todo Use config instead of state.
      '#default_value' => $this->variableGet("aws_region", ""),
      '#states' => [
        'visible' => [
          ':input[name="enable_s3_service"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['aws_version'] = [
      '#type' => 'textfield',
      '#title' => "AWS version",
      '#description' => $this->t("aws version"),
      // @todo Use config instead of state.
      '#default_value' => $this->variableGet("aws_version", ""),
      '#states' => [
        'visible' => [
          ':input[name="enable_s3_service"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['aws_key'] = [
      '#type' => 'password',
      '#title' => "AWS key",
      '#description' => $this->t("aws key"),
      // @todo Use config instead of state.
      '#default_value' => $this->variableGet('aws_key', ''),
      '#states' => [
        'visible' => [
          ':input[name="enable_s3_service"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['aws_secret'] = [
      '#type' => 'password',
      '#title' => "AWS secret",
      '#description' => $this->t("aws secret"),
      // @todo Use config instead of state.
      '#default_value' => $this->variableGet("aws_secret", ""),
      '#states' => [
        'visible' => [
          ':input[name="enable_s3_service"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['aws_bucket'] = [
      '#type' => 'textfield',
      '#title' => "AWS bucket",
      '#description' => $this->t("aws bucket"),
      // @todo Use config instead of state.
      '#default_value' => $this->variableGet("aws_bucket", ""),
      '#states' => [
        'visible' => [
          ':input[name="enable_s3_service"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['aws_bucket_folder'] = [
      '#type' => 'textfield',
      '#title' => "AWS bucket folder",
      '#description' => $this->t("aws bucket folder"),
      // @todo Use config instead of state.
      '#default_value' => $this->variableGet("aws_bucket_folder", ""),
      '#states' => [
        'visible' => [
          ':input[name="enable_s3_service"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    foreach ($form_state->getValues() as $key => $value) {

      $this->variableSet($key, $value);

      $this->config('Dbexport.settings')

        ->save();

      parent::submitForm($form, $form_state);

    }

    $this->messenger->addMessage("updated successfully");

  }

}
