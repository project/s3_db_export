<?php

namespace Drupal\s3_db_export;

/**
 * {@inheritdoc}
 */
interface DbexportManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function dbExport();

  /**
   * {@inheritdoc}
   */
  public function dbDownload();

}
