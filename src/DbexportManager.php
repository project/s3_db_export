<?php

namespace Drupal\s3_db_export;

use Aws\S3\S3Client;
use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;
use Ifsnop\Mysqldump\Mysqldump;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a class for exporting the database and uploading it to AWS S3.
 *
 * @see \Drupal\s3_db_export\DbexportManagerInterface
 */
class DbexportManager implements DbexportManagerInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructor method.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The filesystem service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(Connection $connection, FileSystemInterface $fileSystem, StateInterface $state) {
    $this->connection = $connection;
    $this->fileSystem = $fileSystem;
    $this->state = $state;
  }

  /**
   * Creates a new instance of the DbexportManager class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container instance.
   *
   * @return static
   *   A new instance of the DbexportManager class.
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('database'),
          $container->get('file_system'),
          $container->get('state')
      );
  }

  /**
   * Gets a variable from the state service.
   *
   * @param string $first_param
   *   The name of the variable.
   * @param string $sec_param
   *   The default value to return if the variable is not found.
   *
   * @return mixed
   *   The value of the variable or the default value if not found.
   */
  public function variableGet($first_param, $sec_param = "") {
    // @todo Use config instead of state.
    $r = $this->state->get($first_param);
    return ($r == NULL) ? $sec_param : $r;
  }

  /**
   * Perform a database dump.
   *
   * @return string
   *   The filename of the dumped database.
   */
  public function dump() {
    $connection = $this->connection->getConnectionOptions();

    // @todo We should be using the private directory AND deleting the file
    //   after it's used, I didn't see the file being deleted.
    $directory = $this->fileSystem->realpath("public://tmp/");

    $dump = new Mysqldump('mysql:host=' . $connection["host"] . ';dbname=' . $connection["database"], $connection["username"], $connection["password"]);
    $filename = 'backup_' . time() . rand(0, 999) . '.sql';
    $dump->start($directory . '/' . $filename);
    // \Drupal::logger('s3_db_export')->info('Database Dump Started');
    return $filename;
  }

  /**
   * Export the database to AWS S3.
   */
  public function dbExport() {
    $dump = $this->dump();
    if ($this->variableGet('enable_s3_service')) {
      $this->awss3($dump);
    }
    die;
  }

  /**
   * Download the database dump.
   *
   * @return string
   *   The filename of the downloaded database.
   */
  public function dbDownload() {
    return $this->dump();
  }

  /**
   * Move the database dump to AWS S3.
   *
   * @param string $filename
   *   The filename of the database dump.
   *
   * @todo Rename function to awsS3Upload.
   */
  public function awss3($filename) {
    $directory = $this->fileSystem->realpath("public://tmp/");
    // @todo Use config instead of state.
    $s3 = new S3Client(
          [
            'region' => $this->variableGet("aws_region", ""),
            'version' => $this->variableGet("aws_version", ""),
            'credentials' => [
              'key' => $this->variableGet("aws_key", ""),
              'secret' => $this->variableGet("aws_secret", ""),
            ],
          ]
      );

    // @todo Use config instead of state.
    $aws_bucket_folder = $this->variableGet("aws_bucket_folder", "");
    $file_name = $aws_bucket_folder . '/' . $filename;
    $temp_file_location = $directory . '/' . $filename;

    $result = $s3->putObject(
          [
          // @todo Use config instead of state.
            'Bucket' => $this->variableGet("aws_bucket", ""),
            'Key' => $file_name,
            'SourceFile' => $temp_file_location,
          ]
      );
    if ($result) {
      // \Drupal::logger('s3_db_export')->info('Uploaded database in
      // aws via cron');
    }
    else {
      // \Drupal::logger('s3_db_export')->error('Something went wrong');
    }

    // \Drupal::logger('s3_db_export')->info($result);
  }

}
