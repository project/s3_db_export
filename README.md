## CONTENTS OF THIS FILE

- Introduction
- Key Features
- Requirements
- Installation
- Post Installation Guide
- Maintainers


## INTRODUCTION

The "s3_db_export" Drupal module simplifies database management by automating database exports and securely storing backups in Amazon S3 on daily basis.

## KEY FEATURES:

- **Cron Job Integration:** Seamlessly schedule and automate database exports at your preferred intervals.
- **AWS S3 Integration:** Easily connect your Drupal site to your Amazon S3 bucket for secure and reliable backup storage.
- **Data Protection:** Ensure robust data protection with encrypted data transfer and storage.
- **Custom Scheduling:** Tailor backup schedules to your needs, whether it's daily, weekly, or custom intervals.
- **User-Friendly Interface:** A user-friendly interface for hassle-free management.
- **Detailed Logging and Reporting:** Keep a record of export activities and generate reports for tracking backup status.
- **Direct Backup Route:** Initiate a manual database export directly by using the "/db-export-s3" route.

## REQUIREMENTS

This module requires no modules outside of Drupal core.

## INSTALLATION

- Install the Redirect after login module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.

## POST INSTALLATION GUIDE

Congratulations on installing the "s3_db_export" Drupal module! This guide will walk you through the post-installation steps to set up your module for seamless database exports and secure backups in Amazon S3.

### Step 1: Configure AWS S3 Details

1. Navigate to the configuration page by going to "/admin/config/content/DbForm" in your Drupal admin interface.
2. On the configuration page, you'll find settings to configure your AWS S3 details:
   - **Access Key:** Enter your AWS access key.
   - **Secret Key:** Provide your AWS secret key.
   - **S3 Bucket Name:** Specify the name of the S3 bucket where backups will be stored.
   - **Backup Directory:** Define the directory path within your bucket for storing backups.
   - **Encryption:** Choose whether to enable encryption for backup files.
3. Save your AWS S3 configuration.

### Step 2: Schedule Database Exports

1. Configure the export schedule by selecting your preferred backup frequency (e.g., daily, weekly, custom).
2. Define the time and frequency for automated exports to suit your needs.
3. Save your export schedule settings.

### Step 3: Export Your Database

1. Return to the main module interface.
2. Initiate a manual database export or wait for the scheduled export to run automatically.
3. You can also initiate a manual database export directly by using the "/db-export-s3" route. Simply navigate to "/db-export-s3" in your browser or through your Drupal admin interface.
4. Monitor the export progress and verify that your database backup is securely stored in your configured AWS S3 bucket.

For more information and support, please refer to the module's documentation or community resources.

## MAINTAINERS

- Dharmendra Saini (dharmendra-virasat) - https://www.drupal.org/u/dharmendra-virasat

# Supporting organization

- Virasat Solutions - https://www.drupal.org/virasat-solutions

Expert in Drupal design, development, Theming, and migration, as well as UI/UX design which makes us a one-stop Drupal development company.

